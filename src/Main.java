public class Main {
    public static void main(String[] args) {
        Car car = new Car("Toyota", "Honda", 2019, 50.0);

        // Get the property values using the getter methods
        System.out.println("Make: " + car.getMake());
        System.out.println("Model: " + car.getModel());
        System.out.println("Year: " + car.getYear());
        System.out.println("Rental Price: " + car.getRentalPrice());

        // Modify the property values using the setter methods
        car.setMake("Kia");
        car.setModel("Carens");
        car.setYear(2022);
        car.setRentalPrice(60.0);

        // Get the updated property values
        System.out.println("Make: " + car.getMake());
        System.out.println("Model: " + car.getModel());
        System.out.println("Year: " + car.getYear());
        System.out.println("Rental Price: " + car.getRentalPrice());
    }
}